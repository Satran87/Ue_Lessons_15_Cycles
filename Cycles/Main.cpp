#include <cstdlib>
#include <iostream>

using namespace std;
void printDigits(int data,bool printEven)
{
	cout << "Print even = " << printEven<<endl;
	bool isEven;
	for (int t = 0; t <= data; ++t)
	{
		isEven = t % 2 == 0;
		if (isEven && printEven)
		{
			cout << t << endl;
			continue;
		}
		if (!isEven && !printEven)
		{
			cout << t << endl;
		}
	}
}
int main()
{
	cout << std::boolalpha;
	const int counter(12);
	for(int t=0; t<=counter;++t)
	{
		if (t % 2 == 0)
			cout << t<<endl;
	}
	printDigits(6, true);
	printDigits(8, false);
	return EXIT_SUCCESS;
}
